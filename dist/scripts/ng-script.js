var chosenTab = null;

// Делает вкладку по клику активной
var tabsChange = function () {
  var tabs = document.querySelectorAll('#three .tab');
  tabs.forEach(function(item, index){
    item.onclick = function(){
      tabs.forEach(function(item){
        if(item.classList.contains('active')) {
          item.classList.remove('active');
        }
      });
      this.classList.add('active');
      // Можно будет потом условие привязать к глобальной переменной chosenTab.
      chosenTab = index+1;
    };
  });
};

// меняет цвет индикатора
var indicatorChange = function() {
  var items = document.querySelectorAll('#four tbody .indicator');
  items.forEach(function(item){
    var val = item.innerText.split('/')[0];
    //TODO: Сами поставьте какие значения выводят какой индикатор: ind-r = красный, ind-y = желтый, ind-g = зеленый
    val <= 30 ? item.classList.add('ind-r') : val > 30 && val < 60 ? item.classList.add('ind-y') : item.classList.add('ind-g')
  });
};

// переключатель
// var switcher

function reverse(data) {
  var string = String(data);
  var letters = [],
      l = string.length;
  while (l--) {
    letters.push(string[l]);
  }
  return letters.join('');
};

function addSeparators(data) {
  var string = String(data);
  string = reverse(string).match(/\d{1,3}/g).join(',');
  string = reverse(string);
  return string;
};

// Делает денежные суммы с запятыми-разделителями
var moneyChange = function() {
  var items = document.querySelectorAll('#four tbody .money');
  items.forEach(function(item){
    item.innerText = addSeparators(item.innerText);
  })
};

window.onload = function() {
  tabsChange();
  indicatorChange();
  moneyChange();
  document.querySelector('main .container').style.display = 'block';
  document.querySelector('main').style.opacity = 1;
  document.querySelector('main').style.height = 'auto';
  document.querySelector('.loader').style.display = 'none';

  //TODO: если будете добавлять функции, которые меняют список, то из него будут исчезать индикаторы и запятые у сумм.
  // Добавляйте функции indicatorChange() и moneyChange() к нужным событиям
  document.querySelector('#search').onkeyup = function() {
    indicatorChange();
    moneyChange();
  }
  document.querySelector('.show-more .btn').onclick = function(){
    indicatorChange();
    moneyChange();
  }
};

setTimeout(function(){
   indicatorChange();
   moneyChange();
}, 3000);


angular.module('Mirocana', [])
.config(['$sceDelegateProvider', function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    'self',
    'https://api.mirocana.com/**',
    'http://localhost:3000/**',
    'https://mirocana.com/**'
  ]);
}])
.controller('mainCtrl', ['$scope', '$http', '$templateCache', function($scope, $http, $templateCache){
  //TODO: Надо разбираться с кроссдоменными запросами, чет там замудрено. 
  //Если нужно получать объект со стороннего домена. С текущего работает.

  $scope.url = 'https://cors.io/?https://api.mirocana.com/public_api/target/projects';
  // $scope.url = 'http://localhost:3000/data/data.json';
  
  $http({method: "GET", url: $scope.url, cache: $templateCache})
  .then(function(response) {
      $scope.status = response.status;
      $scope.data = response.data.data;
      indicatorChange();
      moneyChange();  
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });

  $scope.numLimit = 10;
  $scope.checked = false;
  $scope.switchItem2 = '';
  $scope.switchItem1 = 'active';

  $scope.switcher = function() {
    if($scope.checked) {
      $scope.switchItem2 = 'active';
      $scope.switchItem1 = '';
    } else {
      $scope.switchItem2 = '';
      $scope.switchItem1 = 'active';
    }
  };

  //TODO: Для вкладок нужно писать отдельный фильтр и для переключателя тоже.

  $scope.targetNum = chosenTab;
  

}]);