var gulp = require('gulp'),
sass = require('gulp-sass'),
mincss = require('gulp-clean-css'),
rename = require('gulp-rename'),
path = {
    css: 'src/css/*.css',
    sassBuild: ['src/scss/style.scss'],
    sassWatch: ['src/scss/**/*.scss'],
    img: 'src/img/**/*',
    pugWatch: 'src/pug/**/*.pug',
    pugBuild: ['src/pug/*.pug'],
    js: ['src/js/*.js']
},
plumber = require('gulp-plumber'),
uglify = require('gulp-uglify'),
autoprefixer = require('gulp-autoprefixer'),
imagemin = require('gulp-imagemin'),
pug = require('gulp-pug'),
watch = rename('gulp-watch'),
csscomb = require('gulp-csscomb'),
$ = require('gulp-load-plugins')(),
browserSync = require('browser-sync').create();;

//SCSS to CSS + prefixer
gulp.task('sass', function () {
return gulp.src(path.sassBuild)
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe($.csscomb())
    .pipe(gulp.dest('src/css'));
});

//Minify CSS
gulp.task('mincss', ['sass'], function () {
return gulp.src(path.css)
    .pipe(mincss({compatibility: 'ie8'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist/styles'))
    .pipe(browserSync.stream());
});

//Minify Js
gulp.task('minjs', function () {
return gulp.src(path.js)
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist/scripts'));
});

//Minify images
gulp.task('imagemin', function () {
return gulp.src(path.img)
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        optimizationLevel: 5,
        interlaced: true
    }))
    .pipe(gulp.dest('dist/images'));
});

// Pug compilation
gulp.task('html', function () {
return gulp.src(path.pugBuild)
    .pipe(plumber())
    .pipe(pug({pretty: true}))
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.stream());
});

gulp.task('page', ['sass', 'mincss', 'html']);

gulp.task('serve', ['sass', 'mincss', 'html'], function() {

    browserSync.init({
        server: "./dist"
    });

    gulp.watch(path.sassWatch, ['sass', 'mincss']);
    gulp.watch(path.pugWatch, ['html']);
    gulp.watch(path.js, ['minjs']);
    gulp.watch("dist/*.html").on('change', browserSync.reload);
    gulp.watch("dist/styles/*.css").on('change', browserSync.reload);
});

// Gulp watch
gulp.task('watch', function () {
gulp.watch(path.sassWatch, ['sass', 'mincss']);
gulp.watch(path.pugWatch, ['html']);
gulp.watch(path.js, ['minjs']);
});